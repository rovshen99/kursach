from django import forms

from . import models


class PupilForm(forms.ModelForm):
    class Meta:
        model = models.Pupil
        fields = '__all__'


class TeacherForm(forms.ModelForm):
    class Meta:
        model = models.Teacher
        fields = '__all__'


class CourseForm(forms.ModelForm):
    class Meta:
        model = models.Course
        fields = '__all__'
