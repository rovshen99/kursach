from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.contrib.auth.decorators import login_required

from . import models
from . import forms


@login_required
def pupils_view(request):
    search_query = request.GET.get('search', '')
    if search_query:
        pupils = models.Pupil.objects.filter(full_name__icontains=search_query)
    else:
        pupils = models.Pupil.objects.all()
    context = {'pupils': pupils}
    return render(request, 'school/pupil/pupils.html', context)


@login_required
def pupil_detail(request, pk):
    pupil = get_object_or_404(models.Pupil, pk=pk)
    return render(request, 'school/pupil/pupil_detail.html', {'pupil': pupil})


@login_required
def pupil_create(request):
    if request.method == 'POST':
        create = forms.PupilForm(request.POST)
        if create.is_valid():
            create.save()
            fio = create.cleaned_data.get('full_name')
            messages.success(request, f'Ученик: {fio} добавлен в базу данных')
            return redirect('pupils_view')
    else:
        create = forms.PupilForm()
    return render(request, 'school/pupil/pupil_create.html', {'form': create})


@login_required
def pupil_update(request, pk):
    pupil = get_object_or_404(models.Pupil, pk=pk)
    pupil_form = forms.PupilForm(request.POST or None, instance=pupil)
    if pupil_form.is_valid():
        pupil_form.save()
        return redirect('pupils_view')
    return render(request, 'school/pupil/pupil_update.html', {'form': pupil_form})


@login_required
def pupil_delete(request, pk):
    pupil = get_object_or_404(models.Pupil, pk=pk)
    pupil.delete()
    return redirect('pupils_view')


@login_required
def pupil_delete_confirm(request, pk):
    pk = int(pk)
    pupil = get_object_or_404(models.Pupil, pk=pk)
    return render(request, 'school/pupil/pupil_delete_confirm.html', {'pupil': pupil})


@login_required
def teachers_view(request):
    search_query = request.GET.get('search', '')
    if search_query:
        teachers = models.Teacher.objects.filter(full_name__icontains=search_query)
    else:
        teachers = models.Teacher.objects.all()
    context = {'teachers': teachers}
    return render(request, 'school/teacher/teachers.html', context)


@login_required
def teacher_detail(request, pk):
    teacher = get_object_or_404(models.Teacher, pk=pk)
    return render(request, 'school/teacher/teacher_detail.html', {'teacher': teacher})


@login_required
def teacher_create(request):
    if request.method == 'POST':
        create = forms.TeacherForm(request.POST)
        if create.is_valid():
            create.save()
            fio = create.cleaned_data.get('full_name')
            messages.success(request, f'Учитель: {fio} добавлен в базу данных')
            return redirect('teachers_view')
    else:
        create = forms.TeacherForm()
    return render(request, 'school/teacher/teacher_create.html', {'form': create})


@login_required
def teacher_update(request, pk):
    teacher = get_object_or_404(models.Teacher, pk=pk)
    teacher_form = forms.TeacherForm(request.POST or None, instance=teacher)
    if teacher_form.is_valid():
        teacher_form.save()
        return redirect('teachers_view')
    return render(request, 'school/teacher/teacher_update.html', {'form': teacher_form})


@login_required
def teacher_delete(request, pk):
    teacher = get_object_or_404(models.Teacher, pk=pk)
    teacher.delete()
    return redirect('teachers_view')


@login_required
def teacher_delete_confirm(request, pk):
    pk = int(pk)
    teacher = get_object_or_404(models.Teacher, pk=pk)
    return render(request, 'school/teacher/teacher_delete_confirm.html', {'teacher': teacher})


def courses_view(request):
    search_query = request.GET.get('search', '')
    if search_query:
        courses = models.Course.objects.filter(name__icontains=search_query)
    else:
        courses = models.Course.objects.all()
    context = {'courses': courses, }
    return render(request, 'school/course/courses.html', context)


@login_required
def course_detail(request, pk):
    course = get_object_or_404(models.Course, pk=pk)
    return render(request, 'school/course/course_detail.html', {'course': course})


@login_required
def course_create(request):
    if request.method == 'POST':
        create = forms.CourseForm(request.POST)
        if create.is_valid():
            create.save()
            fio = create.cleaned_data.get('name')
            messages.success(request, f'Ученик: {fio} добавлен в базу данных')
            return redirect('courses_view')
    else:
        create = forms.CourseForm()
    return render(request, 'school/course/course_create.html', {'form': create})


@login_required
def course_update(request, pk):
    course = get_object_or_404(models.Course, pk=pk)
    course_form = forms.CourseForm(request.POST or None, instance=course)
    if course_form.is_valid():
        course_form.save()
        return redirect('courses_view')
    return render(request, 'school/course/course_update.html', {'form': course_form})


@login_required
def course_delete(request, pk):
    course = get_object_or_404(models.Course, pk=pk)
    course.delete()
    return redirect('courses_view')


@login_required
def course_delete_confirm(request, pk):
    pk = int(pk)
    course = get_object_or_404(models.Course, pk=pk)
    return render(request, 'school/course/course_delete_confirm.html', {'course': course})
