from django.db import models
from django.utils import timezone
from datetime import date


class Pupil(models.Model):
    SEX_CHOICE = (
        ('m', 'М'),
        ('w', 'Ж'),
    )
    full_name = models.CharField('ФИО', max_length=50, default="Иванов Иван")
    sex = models.CharField('Пол', max_length=2, choices=SEX_CHOICE, default='m')
    birthday = models.DateField('День Рождения', default=timezone.now())

    class Meta:
        verbose_name = "Ученик"
        verbose_name_plural = 'Ученики'

    def get_age(self):
        today = date.today()
        return today.year - self.birthday.year - ((today.month, today.day) < (self.birthday.month, self.birthday.day))

    def __str__(self):
        return self.full_name


class Subject(models.Model):
    name = models.CharField('Предмет', max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Предмет"
        verbose_name_plural = 'Предметы'


class Teacher(models.Model):
    JOB_POSITION = (
        ('d', 'Директор'),
        ('z', 'Завуч'),
        ('u', 'Учитель'),
    )
    full_name = models.CharField('ФИО', max_length=50)
    subject = models.ForeignKey(Subject, verbose_name='Предмет', on_delete=models.SET_NULL, blank=True, null=True)
    job_pos = models.CharField('Должность', max_length=2, choices=JOB_POSITION)

    def __str__(self):
        return self.full_name

    class Meta:
        verbose_name = "Учитель"
        verbose_name_plural = 'Учителя'


class Course(models.Model):
    name = models.CharField('Класс', max_length=10, unique=True)
    teacher = models.ForeignKey(Teacher, verbose_name='Учитель', on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Класс"
        verbose_name_plural = 'Классы'
