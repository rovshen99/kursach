from django.contrib import admin
from . import models

# Register your models here.
@admin.register(models.Pupil)
class PupilAdmin(admin.ModelAdmin):
    list_display = ['full_name', 'sex', 'birthday', 'get_age']
    search_fields = ['full_name']


@admin.register(models.Subject)
class SubjectAdmin(admin.ModelAdmin):
    fields = ['name']


@admin.register(models.Teacher)
class TeacherAdmin(admin.ModelAdmin):
    list_display = ['full_name', 'job_pos', 'subject']


@admin.register(models.Course)
class CourseAdmin(admin.ModelAdmin):
    list_display = ['name', 'teacher']
