"""shop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from school import views as school_views
from user import views as user_views
from django.contrib.auth import views as auth_views
urlpatterns = [
    path('', auth_views.LoginView.as_view(template_name='user/login.html'), name='login_view'),
    path('admin/', admin.site.urls),
    path('pupils/', school_views.pupils_view, name='pupils_view'),
    path('pupil-create/', school_views.pupil_create, name='pupil_create_view'),
    path('pupil-detail/<int:pk>/', school_views.pupil_detail, name='pupil_detail_view'),
    path('pupil-update/<int:pk>/', school_views.pupil_update, name='pupil_update_view'),
    path('pupil-delete/<int:pk>/', school_views.pupil_delete, name='pupil_delete_view'),
    path('pupil-delete-confirm/<int:pk>/', school_views.pupil_delete_confirm, name='pupil_delete_confirm'),
    path('teachers/', school_views.teachers_view, name='teachers_view'),
    path('teacher-detail/<int:pk>/', school_views.teacher_detail, name='teacher_detail_view'),
    path('teacher-create/', school_views.teacher_create, name='teacher_create_view'),
    path('teacher-update/<int:pk>/', school_views.teacher_update, name='teacher_update_view'),
    path('teacher-delete/<int:pk>/', school_views.teacher_delete, name='teacher_delete_view'),
    path('teacher-delete-confirm/<int:pk>/', school_views.teacher_delete_confirm, name='teacher_delete_confirm'),
    path('courses/', school_views.courses_view, name="courses_view"),
    path('course-detail/<int:pk>/', school_views.course_detail, name='course_detail_view'),
    path('course-create/', school_views.course_create, name='course_create_view'),
    path('course-update/<int:pk>/', school_views.course_update, name='course_update_view'),
    path('course-delete/<int:pk>/', school_views.course_delete, name='course_delete_view'),
    path('course-delete-confirm/<int:pk>/', school_views.course_delete_confirm, name='course_delete_confirm'),
    path('register/', user_views.register_view, name='register_view'),
    path('login/', auth_views.LoginView.as_view(template_name='user/login.html'), name='login_view'),
    path('logout/', auth_views.LogoutView.as_view(template_name='user/logout.html'), name='logout_view'),

]
