"""
WSGI config for kursach1 project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJAdNGO_SETTINGS_MODULE', 'kursach1.settings')

application = get_wsgi_application()
